package com.springdemo.dbdemo;

import com.springdemo.dbdemo.entity.StoreInformation;
import com.springdemo.dbdemo.repository.StoreInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbDemoApplication implements CommandLineRunner {

    @Autowired
    StoreInformationRepository storeInformationRepository;

    public static void main(String[] args) {
        SpringApplication.run(DbDemoApplication.class, args);
    }

    @Override
    public void run(String... args){
        StoreInformation storeInformationOne=new StoreInformation("Amazon","All types of bags are available here","xxxxx");
        StoreInformation storeInformationTwo=new StoreInformation("Flipkart","All types of books are available here","yyyyy");
        StoreInformation storeInformationThree=new StoreInformation("Meesho","All fashion items are available here","zzzzz");
        StoreInformation storeInformationFour=new StoreInformation("Myntra","All fashion items are available here","aaaaa");

        storeInformationRepository.save(storeInformationOne);
        storeInformationRepository.save(storeInformationTwo);
        storeInformationRepository.save(storeInformationThree);
        storeInformationRepository.save(storeInformationFour);

        storeInformationRepository.findByStoreName("Amazon").forEach(
                val ->System.out.println(val)
        );
        storeInformationRepository.findByStoreDetail("All fashion items are available here").forEach(
                val ->System.out.println(val)
        );
        storeInformationRepository.findByStorePhoneNumber("yyyyy").forEach(
                val ->System.out.println(val)
        );
        storeInformationRepository.findById(2).ifPresent(
                val ->System.out.println(val)
        );

        System.out.println(storeInformationRepository.count());

        storeInformationRepository.deleteById(2);

        System.out.println(storeInformationRepository.count());

    }
}
